/*
 * @Descripttion: 首页配置
 * @version: 1.0.0
 * @Author: Velen
 * @Date: 2019-09-02 14:06:59
 * @LastEditors: Velen
 * @LastEditTime: 2019-09-02 14:58:32
 */
import { Component, Vue } from 'vue-property-decorator'
import { Getter, Action } from 'vuex-class'
import { HomeData } from '@/types/views/Home.interface'
// import {  } from '@/components' // 组件

@Component({})
export default class About extends Vue {
  // Getter
  // @Getter Home.author
  // Action
  // @Action GET_DATA_ASYN
  // data
  public data: HomeData = {
    pageName: 'Home'
  }
  public created() {
    //
  }
  public activated() {
    //
  }
  public mounted() {
    //
  }
  // 初始化函数
  public init() {
    //
  }
}
