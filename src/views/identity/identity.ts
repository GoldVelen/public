/*
 * @Descripttion: 注册登录
 * @version: 1.0.0
 * @Author: Velen
 * @Date: 2019-09-05 09:23:42
 * @LastEditors: Velen
 * @LastEditTime: 2019-09-06 14:45:18
 */
import { Component, Prop, Vue } from 'vue-property-decorator'
// import request from '../utils/request'
@Component
export default class Identity extends Vue {
  /**
   * Name: 检查函数
   */
  private validatePass = (rule: object, value: string, callback: any) => {
    if (value === '') {
      callback(new Error('请输入密码'))
    } else {
      if (this.ruleForm.checkPass !== '') {
        (this.$refs[this.activeName] as any).validateField('checkPass')
      }
      callback()
    }
  }
  private validatePass2 = (rule: object, value: string, callback: any) => {
    if (value === '') {
      callback(new Error('请再次输入密码'))
    } else if (value !== this.ruleForm.pass) {
      callback(new Error('两次输入密码不一致!'))
    } else {
      callback()
    }
  }
  /**
   * Name: data数据
   */
  private ruleForm = {
    pass: '',
    checkPass: '',
    nickname: '',
    email: '',
    code: ''
  }
  private loginForm = {
    pass: '',
    email: ''
  }
  private sendStatus = '发送'
  protected num = 59
  private activeName = 'register'
  /**
   * name: 规则配置
   * Note: 这里会有一个私有声明不能再被公共声明的lint错误，修改了lint。json里的member-ordering
   */
  private rules = {
    pass: [
      {
        required: true,
        validator: this.validatePass,
        trigger: ['blur', 'change']
      }
    ],
    checkPass: [
      {
        required: true,
        validator: this.validatePass2,
        trigger: ['blur', 'change']
      }
    ],
    nickname: [
      { required: true, message: '请输入昵称', trigger: 'blur' },
      { min: 3, max: 5, message: '长度在 3 到 5 个字符', trigger: 'change' }
    ],
    email: [
      { required: true, message: '请输入邮箱地址', trigger: 'blur' },
      {
        type: 'email',
        message: '请输入正确的邮箱地址',
        trigger: ['blur', 'change']
      }
    ],
    code: [
      {
        required: true,
        message: '验证码不能为空',
        trigger: ['blur', 'change']
      }
    ]
  }
  /**
   * @name: 发送验证码
   * @return: NULL
   */
  private sendCode() {
    // 校验email成功后开始发送
    const key: string = this.activeName;
    (this.$refs[key] as any).validateField('email', async (err: string) => {
      if (!err) {
        // 点击过后禁止点击
        if (this.sendStatus !== '发送') {
          return
        }
        // 计时
        this.sendStatus = this.num + 's'
        const time = setInterval(() => {
          if (this.num <= 0) {
            this.num = 59
            this.sendStatus = '发送'
            clearInterval(time)
            return
          }
          this.num -= 1
          this.sendStatus = this.num + 's'
        }, 1000)
        const result = await Vue.prototype.request.sendEmail({
          email: this.ruleForm.email,
          type: 0
        })
      }
    })
  }
  /**
   * @name: 表单校验
   * @return: boolean
   */
  private submitForm(formName: string) {
    (this.$refs[formName] as any).validate(async (valid: boolean) => {
      if (valid) {
        console.log(this.activeName)
        const paramsKeys =  this.activeName === 'register' ? {
          code: Number(this.ruleForm.code) || 0,
          email: this.ruleForm.email,
          nickname: this.ruleForm.nickname,
          password: this.ruleForm.pass
        } : {
          email: this.ruleForm.email,
          password: this.ruleForm.pass
        }
        const result = this.activeName === 'register'
          ? await Vue.prototype.request.register(paramsKeys, 'POST')
          : await Vue.prototype.request.login(paramsKeys, 'POST')
        console.log(result)
      } else {
        return false
      }
    })
  }
  /**
   * @name: 登录
   * @return: NULL
   */
  private handleClick() {
    console.log(this.activeName)
    this.resetForm(this.activeName)
  }
  /**
   * @name: 重置表单
   * @return: NULL
   */
  private resetForm(formName: string) {
    (this.$refs[formName] as any).resetFields()
  }
}
