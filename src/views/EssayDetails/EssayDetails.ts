/*
 * @Descripttion: 文章详情配置
 * @version: 1.0.0
 * @Author: Velen
 * @Date: 2019-09-02 14:17:24
 * @LastEditors: Velen
 * @LastEditTime: 2019-09-02 15:07:23
 */
import { Component, Vue } from 'vue-property-decorator'
import { Getter, Action } from 'vuex-class'
import { EssayDetailsData } from '@/types/views/EssayDetails.interface'
// import {  } from '@/components' // 组件

@Component({})
export default class About extends Vue {
  // Getter
  // @Getter EssayDetails.author
  // Action
  // @Action GET_DATA_ASYN
  // data
  public data: EssayDetailsData = {
    pageName: 'EssayDetails'
  }
  public created() {
    //
  }
  public activated() {
    //
  }
  public mounted() {
    //
  }
  // 初始化函数
  public init() {
    //
  }
}
