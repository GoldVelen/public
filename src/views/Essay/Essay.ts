/*
 * @Descripttion: 文章配置
 * @version: 1.0.0
 * @Author: Velen
 * @Date: 2019-09-02 14:05:42
 * @LastEditors: Velen
 * @LastEditTime: 2019-09-02 15:08:31
 */
import { Component, Vue } from 'vue-property-decorator'
import { Getter, Action } from 'vuex-class'
import { EssayData } from '@/types/views/Essay.interface'
// import {  } from '@/components' // 组件

@Component({})
export default class About extends Vue {
  // Getter
  // @Getter Essay.author
  // Action
  // @Action GET_DATA_ASYN
  // data
  public data: EssayData = {
    pageName: 'Essay'
  }
  public created() {
    //
  }
  public activated() {
    //
  }
  public mounted() {
    //
  }
  // 初始化函数
  public init() {
    //
  }
}
