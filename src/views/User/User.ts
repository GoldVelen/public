/*
 * @Descripttion:个人中心配置
 * @version: 1.0.0
 * @Author: Velen
 * @Date: 2019-09-09 09:44:30
 * @LastEditors: Velen
 * @LastEditTime: 2019-09-09 16:51:11
 */
import { Component, Vue } from 'vue-property-decorator'
import { Getter, Action } from 'vuex-class'
import { UserData } from '@/types/views/User.interface'
import Articles from '@/components/Articles/Articles.vue';
import VueEmoji from 'emoji-vue';
// import {  } from '@/components' // 组件

@Component({
  components: {
    Articles, VueEmoji
  },
})
export default class About extends Vue {
  // Getter
  // @Getter User.author
  // Action
  // @Action GET_DATA_ASYN
  // data
  private listing: any = [
    { img: '/img/zhanweitu.jpg', name: '用户姓名', content: '用户留言内容', time: '2019-10-22' },
    { img: '/img/zhanweitu.jpg', name: '用户姓名', content: '用户留言内容', time: '2019-10-22' },
  ]
  private labelist: any = [
    { num: '112', title: '文章' },
    { num: '200', title: '评论' },
    { num: '1201', title: '浏览' },
    { num: '55', title: '点赞' },
  ]
  private myText: string = '说点什么呢~~~'
  private remlyShow: any = false
  private editShow: any = false
  private id: number = 0
  private form: any = {
    imageUrl: '',
    name: '',
    WeChat: '',
    vocation: '',
    type: [],
    Music: '',
    desc: ''
  }
  public reply(index: number) {
    console.log(index)
    this.id = index
    console.log(this.id)
    if (this.id === index) {
      this.remlyShow = true
    } else {
      this.remlyShow = false
    }
  }
  // 表情包输入框内容
  public onInput(event: any) {
    console.log(event.data);
  }
  public clearTextarea() {
    // this.$refs.emoji.clear()
  }
  // 个人信息编辑
  public Edit() {
    this.editShow = !this.editShow
  }
  // 个人信息编辑取消
  public editCancel() {
    this.editShow = !this.editShow
  }
  // 上传图像
  public handleAvatarSuccess(res: any, file: any) {
    this.form.imageUrl = URL.createObjectURL(file.raw);
  }
  // 判断图片格式以及大小
  public beforeAvatarUpload(file: any) {
    // const isJPG = file.type === 'image/jpeg';
    // const isLt2M = file.size / 1024 / 1024 < 2;

    // if (!isJPG) {
    //   this.$message.error('上传头像图片只能是 JPG 格式!');
    // }
    // if (!isLt2M) {
    //   this.$message.error('上传头像图片大小不能超过 2MB!');
    // }
    // return isJPG && isLt2M;
  }
  public onSubmit() {
    console.log('submit!');
  }
  public data: UserData = {
    pageName: 'User'
  }
  public created() {
    //
  }
  public activated() {
    //
  }
  public mounted() {
    //
  }
  // 初始化函数
  public init() {
    //
  }
}
