/*
 * @Descripttion: 留言配置
 * @version: 1.0.0
 * @Author: Velen
 * @Date: 2019-08-30 10:49:35
 * @LastEditors: Velen
 * @LastEditTime: 2019-09-07 17:20:26
 */
import { Component, Vue } from 'vue-property-decorator'
import { Getter, Action } from 'vuex-class'
import { MessageData } from '@/types/views/message.interface'
import Messagecon from '@/components/Message/Message.vue'
@Component({
  components: {
    Messagecon
  }
})
export default class Message extends Vue {
  // Getter
  // @Getter message.author
  // Action
  // @Action GET_DATA_ASYN
  // data
  public data: MessageData = {
    pageName: 'message'
  }
  public created() {
    //
  }
  public activated() {
    //
  }
  public mounted() {
    //
  }
  // 初始化函数
  public init() {
    //
  }
}
