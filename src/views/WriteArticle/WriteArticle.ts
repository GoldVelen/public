/*
 * @Descripttion:
 * @version: 1.0.0
 * @Author: Velen
 * @Date: 2019-09-09 16:10:17
 * @LastEditors: Velen
 * @LastEditTime: 2019-09-20 11:25:39
 */
import { Component, Prop, Vue } from 'vue-property-decorator'
import { Getter, Action } from 'vuex-class'
import { WriteArticleData } from '@/types/views/WriteArticle.interface'
import { VueEditor } from 'vue2-editor'

@Component({
  components: {
    VueEditor
  },
})
export default class WriteArticle extends Vue {
  private form = {
    title: '',
    content: '',
    tags: '',
  }
  private taglist = [
    { label: '技术杂谈', value: '技术杂谈' },
    { label: '生活笔记', value: '生活笔记' },
    { label: 'Python', value: 'Python' },
    { label: 'Html', value: 'Html' },
    { label: 'css', value: 'css' },
    { label: 'javascript', value: 'javascript' },
    { label: 'typescript', value: 'typescript' },
    { label: 'php', value: 'php' },
    { label: 'Mysql', value: 'Mysql' },
    { label: 'jQuery', value: 'jQuery' },
  ]
  private rules = {
    title: [
      { required: true, message: '请输入文章标题', trigger: 'blur' },
    ],
    content: [
      { required: true, message: '请输入文章内容', trigger: 'blur' },
    ],
    tags: [
      { required: true, message: '请选择文章标签', trigger: 'blur' }
    ]
  }
  public onSubmit(formName: string) {
    (this.$refs[formName] as any).validate(async (valid: boolean) => {
      if (valid) {
        const res = await Vue.prototype.request.release(this.form, 'POST')
        if (res.message === '发布成功') {
          (this.$refs[formName] as any).resetFields();
        }
        console.log('submit!' + res);
      } else {
        console.log('error submit!!');
        return false;
      }
    });
  }
  public editCancel() {
    console.log('editCancel!');
  }
  public data: WriteArticleData = {
    pageName: 'WriteArticle'
  }
  public created() {
    //
  }
  public activated() {
    //
  }
  public mounted() {
    //
  }
  // 初始化函数
  public init() {
    //
  }
}
