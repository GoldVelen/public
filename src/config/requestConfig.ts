/*
 * @Descripttion: mock
 * @version: 1.0.0
 * @Author: Velen
 * @Date: 2019-08-28 14:36:09
 * @LastEditors: Velen
 * @LastEditTime: 2019-09-05 16:00:33
 */
export default {
  sendEmail: '/api/identity/sendMail', // 发送邮箱
  register: '/api/identity/register', // 注册
  login: '/api/identity/login', // 注册
}
