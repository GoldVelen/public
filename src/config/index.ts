/*
 * @Descripttion: 不同环境设定不同代理
 * @version: 1.0.0
 * @Author: Velen
 * @Date: 2019-08-28 14:36:00
 * @LastEditors: Velen
 * @LastEditTime: 2019-08-28 15:49:53
 */
/*
 * 线上环境
 */
export const ONLINEHOST: string = 'https://xxx.com'

/*
 * 测试环境
 */
export const QAHOST: string = 'http://xxx.com'

/*
 * 线上mock
 */
export const MOCKHOST: string = 'http://xxx.com'

/**
 * 是否mock
 */
export const ISMOCK: boolean = false

/**
 * 当前的host  ONLINEHOST | QAHOST | MOCKHOST
 */
export const MAINHOST: string = ONLINEHOST

/**
 * 请求的公共参数
 */
export const conmomPrams: any = {}

/**
 * @description token在Cookie中存储的天数，默认1天
 */
export const cookieExpires: number = 1
