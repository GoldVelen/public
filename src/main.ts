/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-08-12 15:03:25
 * @LastEditTime: 2019-09-06 14:38:14
 * @LastEditors: Velen
 */
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index'
import request from './utils/request'
import './plugins/element.js'
import 'element-ui/lib/theme-chalk/base.css'
Vue.config.productionTip = false

Vue.prototype.request = request
new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app')
