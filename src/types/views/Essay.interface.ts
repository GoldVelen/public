// Essay.Data 参数类型
export interface EssayData {
  pageName: string
}

// VUEX Essay.State 参数类型
export interface EssayState {
  [data: string]: any
}

// GET_DATA_ASYN 接口参数类型
// export interface DataOptions {}

