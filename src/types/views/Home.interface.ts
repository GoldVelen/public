/*
 * @Descripttion: 首页配置
 * @version: 1.0.0
 * @Author: Velen
 * @Date: 2019-09-02 14:06:59
 * @LastEditors: Velen
 * @LastEditTime: 2019-09-02 15:08:59
 */
// Home.Data 参数类型
export interface HomeData {
  pageName: string
}

// VUEX Home.State 参数类型
export interface HomeState {
  [data: string]: any
}

// GET_DATA_ASYN 接口参数类型
// export interface DataOptions {}

