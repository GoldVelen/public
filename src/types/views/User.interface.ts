/*
 * @Descripttion:个人中心配置
 * @version: 1.0.0
 * @Author: Velen
 * @Date: 2019-09-09 09:44:30
 * @LastEditors: Velen
 * @LastEditTime: 2019-09-09 10:38:41
 */
// User.Data 参数类型
export interface UserData {
  pageName: string
}

// VUEX User.State 参数类型
export interface UserState {
  [data: string]: any
}

// GET_DATA_ASYN 接口参数类型
// export interface DataOptions {}

