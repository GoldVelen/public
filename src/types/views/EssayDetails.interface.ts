// EssayDetails.Data 参数类型
export interface EssayDetailsData {
  pageName: string
}

// VUEX EssayDetails.State 参数类型
export interface EssayDetailsState {
  [data: string]: any
}

// GET_DATA_ASYN 接口参数类型
// export interface DataOptions {}

