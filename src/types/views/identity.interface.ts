/*
 * @Descripttion: 注册登录的命名空间
 * @version: 1.0.0
 * @Author: Velen
 * @Date: 2019-08-28 16:45:21
 * @LastEditors: Velen
 * @LastEditTime: 2019-09-05 10:49:58
 */
// Identity.Data 参数类型
export interface IdentityData {
  pageName: string
}

// VUEX Identity.State 参数类型
export interface IdentityState {
  data?: any
}

// GET_DATA_ASYN 接口参数类型
// export interface DataOptions {}
