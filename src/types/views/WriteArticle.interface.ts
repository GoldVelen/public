/*
 * @Descripttion:写文章配置
 * @version: 1.0.0
 * @Author: Velen
 * @Date: 2019-09-09 16:10:17
 * @LastEditors: Velen
 * @LastEditTime: 2019-09-09 16:14:43
 */
// WriteArticle.Data 参数类型
export interface WriteArticleData {
  pageName: string
}

// VUEX WriteArticle.State 参数类型
export interface WriteArticleState {
  [data: string]: any
}

// GET_DATA_ASYN 接口参数类型
// export interface DataOptions {}

