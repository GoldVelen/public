/*
 * @Descripttion: 留言板配置
 * @version: 1.0.0
 * @Author: Velen
 * @Date: 2019-08-30 10:49:35
 * @LastEditors: Velen
 * @LastEditTime: 2019-09-02 14:14:57
 */
// message.Data 参数类型
export interface MessageData {
  pageName: string
}

// VUEX message.State 参数类型
export declare interface MessageState {
  [data: string]: any
}

// GET_DATA_ASYN 接口参数类型
// export interface DataOptions {}

