/*
 * @Description: 关于axios请求的配置：拦截器、基本URL和一些封装
 * @Author: Velen
 * @Date: 2019-08-28 10:58:04
 * @LastEditTime: 2019-08-30 15:20:13
 * @LastEditors: Velen
 */
import Vue from 'vue'
import axios from 'axios'
// import VueAxios from 'vue-axios'
// Vue.use(VueAxios, axios)
Vue.use((v) => {
  v.prototype.$axios = axios
})

axios.defaults.baseURL = 'localhost:3000/api/'
axios.defaults.headers.common['Content-Type'] =
  'application/x-www-form-urlencoded'

axios.interceptors.request.use(
  (config: object) => {
    const configData = config
    return configData
  },
  (error: string) => {
    // 对请求错误做些什么
    return Promise.reject(error)
  }
)
axios.interceptors.response.use(
  (response: any) => {
    return response
  },
  (error: string) => {
    // 对请求错误做些什么
    return Promise.reject(error)
  }
)
export default {}
