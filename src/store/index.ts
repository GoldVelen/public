/*
 * @Descripttion: Vuex配置
 * @version: 1.0.0
 * @Author: Velen
 * @Date: 2019-08-28 14:46:00
 * @LastEditors: Velen
 * @LastEditTime: 2019-09-06 11:18:37
 */
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // 全局loading
    acceptLoading: false
  },
  mutations: {
    // 改变全局loading
    updateLoadingStatus(state) {
      state.acceptLoading = !state.acceptLoading
    }
  },
  actions: {
    //
  },
  modules: {
    //
  }
})
