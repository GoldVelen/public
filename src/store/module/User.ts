/*
 * @Descripttion:个人中心配置
 * @version: 1.0.0
 * @Author: Velen
 * @Date: 2019-09-09 09:44:30
 * @LastEditors: Velen
 * @LastEditTime: 2019-09-09 10:39:20
 */
import { UserState } from '@/types/views/User.interface'
import { GetterTree, MutationTree, ActionTree } from 'vuex'
import * as UserApi from '@/api/User'

const OverallState: UserState = {
  User: {
   author: undefined
  }
}

// 强制使用getter获取state
const getters: GetterTree<UserState, any> = {
  author: (state: UserState) => state.User.author
}

// 更改state
const mutations: MutationTree<UserState> = {
  // 更新state都用该方法
  UPDATE_STATE(state: UserState, data: UserState) {
    for (const key in data) {
      if (!data.hasOwnProperty(key)) { return }
      state[key] = data[key]
    }
  }
}

const actions: ActionTree<UserState, any> = {
  UPDATE_STATE_ASYN({ commit, state }, data: UserState) {
    commit('UPDATE_STATE', data)
  },
  // GET_DATA_ASYN({ commit, state: LoginState }) {
  //   User.getData()
  // }
}

export default {
  OverallState,
  getters,
  mutations,
  actions
}

