/*
 * @Descripttion:
 * @version: 1.0.0
 * @Author: Velen
 * @Date: 2019-09-09 16:10:17
 * @LastEditors: Velen
 * @LastEditTime: 2019-09-20 09:47:03
 */
import { WriteArticleState } from '@/types/views/WriteArticle.interface'
import { GetterTree, MutationTree, ActionTree } from 'vuex'
import * as WriteArticleApi from '@/api/WriteArticle'

const OverallState: WriteArticleState = {
  WriteArticle: {
   author: undefined
  }
}

// 强制使用getter获取state
const getters: GetterTree<WriteArticleState, any> = {
  author: (state: WriteArticleState) => state.WriteArticle.author
}

// 更改state
const mutations: MutationTree<WriteArticleState> = {
  // 更新state都用该方法
  UPDATE_STATE(state: WriteArticleState, data: WriteArticleState) {
    for (const key in data) {
      if (!data.hasOwnProperty(key)) { return }
      state[key] = data[key]
    }
  }
}

const actions: ActionTree<WriteArticleState, any> = {
  UPDATE_STATE_ASYN({ commit, state }, data: WriteArticleState) {
    commit('UPDATE_STATE', data)
  },
  // GET_DATA_ASYN({ commit, state: LoginState }) {
  //   WriteArticle.getData()
  // }
}

export default {
  OverallState,
  getters,
  mutations,
  actions
}

