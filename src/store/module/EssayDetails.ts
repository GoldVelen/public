import { EssayDetailsState } from '@/types/views/EssayDetails.interface'
import { GetterTree, MutationTree, ActionTree } from 'vuex'
import * as EssayDetailsApi from '@/api/EssayDetails'

const OverallState: EssayDetailsState = {
  EssayDetails: {
   author: undefined
  }
}

// 强制使用getter获取state
const getters: GetterTree<EssayDetailsState, any> = {
  author: (state: EssayDetailsState) => state.EssayDetails.author
}

// 更改state
const mutations: MutationTree<EssayDetailsState> = {
  // 更新state都用该方法
  UPDATE_STATE(state: EssayDetailsState, data: EssayDetailsState) {
    for (const key in data) {
      if (!data.hasOwnProperty(key)) { return }
      state[key] = data[key]
    }
  }
}

const actions: ActionTree<EssayDetailsState, any> = {
  UPDATE_STATE_ASYN({ commit, state }, data: EssayDetailsState) {
    commit('UPDATE_STATE', data)
  },
  // GET_DATA_ASYN({ commit, state: LoginState }) {
  //   EssayDetails.getData()
  // }
}

export default {
  OverallState,
  getters,
  mutations,
  actions
}

