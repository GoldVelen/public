/*
 * @Descripttion: 文章配置
 * @version: 1.0.0
 * @Author: Velen
 * @Date: 2019-09-02 14:05:42
 * @LastEditors: Velen
 * @LastEditTime: 2019-09-02 14:55:14
 */
import { EssayState } from '@/types/views/Essay.interface'
import { GetterTree, MutationTree, ActionTree } from 'vuex'
import * as EssayApi from '@/api/Essay'

const OverallState: EssayState = {
  Essay: {
   author: undefined
  }
}

// 强制使用getter获取state
const getters: GetterTree<EssayState, any> = {
  author: (state: EssayState) => state.Essay.author
}

// 更改state
const mutations: MutationTree<EssayState> = {
  // 更新state都用该方法
  UPDATE_STATE(state: EssayState, data: EssayState) {
    for (const key in data) {
      if (!data.hasOwnProperty(key)) { return }
      state[key] = data[key]
    }
  }
}

const actions: ActionTree<EssayState, any> = {
  UPDATE_STATE_ASYN({ commit, state }, data: EssayState) {
    commit('UPDATE_STATE', data)
  },
  // GET_DATA_ASYN({ commit, state: LoginState }) {
  //   Essay.getData()
  // }
}

export default {
  OverallState,
  getters,
  mutations,
  actions
}

