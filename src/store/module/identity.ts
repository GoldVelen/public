/*
 * @Descripttion: 注册登录
 * @version: 1.0.0
 * @Author: Velen
 * @Date: 2019-08-28 16:45:21
 * @LastEditors: Velen
 * @LastEditTime: 2019-09-05 10:50:27
 */
import { IdentityState } from '@/types/views/identity.interface'
import { GetterTree, MutationTree, ActionTree } from 'vuex'
import * as IdentityApi from '@/api/identity'

const OverallState: IdentityState = {
  data: {
   author: undefined
  }
}

// 强制使用getter获取state
const getters: GetterTree<IdentityState, any> = {
  author: (state: IdentityState) => state.data.author
}

// 更改state
const mutations: MutationTree<IdentityState> = {
  // 更新state都用该方法
  UPDATE_STATE(state: IdentityState, data: IdentityState) {
    for (const key in data) {
      if (!data.hasOwnProperty(key)) { return }
      state.data[key] = data.data[key]
    }
  }
}

const actions: ActionTree<IdentityState, any> = {
  UPDATE_STATE_ASYN({ commit, state }, data: IdentityState) {
    commit('UPDATE_STATE', data)
  },
  // GET_DATA_ASYN({ commit, state: IdentityState }) {
  //   Identity.getData()
  // }
}

export default {
  OverallState,
  getters,
  mutations,
  actions
}

