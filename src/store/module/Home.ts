/*
 * @Descripttion: 首页配置
 * @version: 1.0.0
 * @Author: Velen
 * @Date: 2019-09-02 14:06:59
 * @LastEditors: Velen
 * @LastEditTime: 2019-09-02 15:09:41
 */
import { HomeState } from '@/types/views/Home.interface'
import { GetterTree, MutationTree, ActionTree } from 'vuex'
import * as HomeApi from '@/api/Home'

const OverallState: HomeState = {
  Home: {
    author: undefined
  }
}

// 强制使用getter获取state
const getters: GetterTree<HomeState, any> = {
  author: (state: HomeState) => state.Home.author
}

// 更改state
const mutations: MutationTree<HomeState> = {
  // 更新state都用该方法
  UPDATE_STATE(state: HomeState, data: HomeState) {
    for (const key in data) {
      if (!data.hasOwnProperty(key)) { return }
      state[key] = data[key]
    }
  }
}

const actions: ActionTree<HomeState, any> = {
  UPDATE_STATE_ASYN({ commit, state}, data: HomeState) {
    commit('UPDATE_STATE', data)
  },
  // GET_DATA_ASYN({ commit, state: LoginState }) {
  //   Home.getData()
  // }
}

export default {
  OverallState,
  getters,
  mutations,
  actions
}

