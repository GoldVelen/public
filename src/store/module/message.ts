/*
 * @Descripttion: 留言板配置
 * @version: 1.0.0
 * @Author: Velen
 * @Date: 2019-08-30 10:49:35
 * @LastEditors: Velen
 * @LastEditTime: 2019-09-02 14:33:59
 */
import { MessageState } from '@/types/views/message.interface'
import { GetterTree, MutationTree, ActionTree } from 'vuex'
import * as MessageApi from '@/api/message'

const OverallState: MessageState = {
  message: {
   author: undefined
  }
}

// 强制使用getter获取state
const getters: GetterTree<MessageState, any> = {
  author: (state: MessageState) => state.message.author
}

// 更改state
const mutations: MutationTree<MessageState> = {
  // 更新state都用该方法
  UPDATE_STATE(state: MessageState, data: MessageState) {
    for (const key in data) {
      if (!data.hasOwnProperty(key)) { return }
      state[key] = data[key]
    }
  }
}

const actions: ActionTree<MessageState, any> = {
  UPDATE_STATE_ASYN({ commit, state}, data: MessageState) {
    commit('UPDATE_STATE', data)
  },
  // GET_DATA_ASYN({ commit, state: 'LoginState'}) {
  //   MessageApi.getData()
  // }
}

export default {
  OverallState,
  getters,
  mutations,
  actions
}

