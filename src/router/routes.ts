/*
 * @Descripttion: 路由跳转配置页
 * @version: 1.0.0
 * @Author: Velen
 * @Date: 2019-08-28 14:45:32
 * @LastEditors: Velen
 * @LastEditTime: 2019-09-06 14:44:09
 * meta 可配置参数
 * @param {boolean} icon 页面icon
 * @param {boolean} keepAlive 是否缓存页面
 * @param {string} title 页面标题
 */
export default [
  {
    path: '/',
    redirect: '/index',
  },
  {
    path: '/index',
    name: 'index',
    component: () => import('../views/Home/Home.vue'),
    meta: {
      icon: '',
      keepAlive: false,
      title: 'index'
    }
  },
  {
    path: '/identity',
    name: 'identity',
    component: () => import('../views/identity/identity.vue'),
    meta: {
      icon: '',
      keepAlive: false,
      title: 'index'
    }
  },
  {
    path: '/essay',
    name: 'essay',
    component: () => import('../views/Essay/Essay.vue'),
    meta: {
      title: '文章分类',
    },
  },
  {
    path: '/essayDetails',
    name: 'essayDetails',
    component: () => import('../views/EssayDetails/EssayDetails.vue'),
    meta: {
      title: '文章详情',
    },
  },
  {
    path: '/message',
    name: 'message',
    component: () => import('../views/message/message.vue'),
    meta: {
      title: '留言板',
    },
  },
]
